

A very simple no thrills logout program. it has 5 different modes that 
can be used to add buttons to an application menu with command line 
arguments: shutdown, reboot, suspend, logout, and key bindings for menu.

the program is entirely operated through keyboard inputs without the 
need for a mouse or touchpad. each action is accessed by entering 
a number in the console rather than navigating through 
the menu with up and down keys.

more information available in the source file.


to compile the program

    Install rust: https://www.rust-lang.org/tools/install

    open the folder containing "tui-session.rs" in the terminal.

    enter in: rustc tui-session.rs



usage


    Put the binary file in your awesome config folder.

    In rc. lua, under mymainmenu add the following:

    { "shutdown", terminal .. " -e /home/finger/.config/awesome/tui-session shutdown" },

    Do the same for the other 3 arguments.


arguments:

        shutdown

        reboot

        suspend

        logout

        menu



set up a hotkey for the menu option:

        awful.key({ modkey }, "q", function () awful.util.spawn(terminal .. " -e /home/finger/.config/awesome/tui-session menu") end,
          {description = "power", group = "awesome"}),



