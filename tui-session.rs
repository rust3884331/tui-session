use std::{thread, time::Duration};
use std::process::Command;
use std::env;
use std::io;

fn commands(y: &str) {
// whatever you put here shows up on every part of the program.
let x = "

    ⠀⠀⠀⠀⠀⢀⣤⠖⠒⠒⠒⢒⡒⠒⠒⠒⠒⠒⠲⠦⠤⢤⣤⣄⣀⠀⠀⠀⠀⠀
    ⠀⠀⠀⠀⣠⠟⠀⢀⠠⣐⢭⡐⠂⠬⠭⡁⠐⠒⠀⠀⣀⣒⣒⠐⠈⠙⢦⣄⠀⠀
    ⠀⠀⠀⣰⠏⠀⠐⠡⠪⠂⣁⣀⣀⣀⡀⠰⠀⠀⠀⢨⠂⠀⠀⠈⢢⠀⠀⢹⠀⠀
    ⠀⣠⣾⠿⠤⣤⡀⠤⡢⡾⠿⠿⠿⣬⣉⣷⠀⠀⢀⣨⣶⣾⡿⠿⠆⠤⠤⠌⡳⣄
    ⣰⢫⢁⡾⠋⢹⡙⠓⠦⠤⠴⠛⠀⠀⠈⠁⠀⠀⠀⢹⡀⠀⢠⣄⣤⢶⠲⠍⡎⣾
    ⢿⠸⠸⡇⠶⢿⡙⠳⢦⣄⣀⠐⠒⠚⣞⢛⣀⡀⠀⠀⢹⣶⢄⡀⠀⣸⡄⠠⣃⣿
    ⠈⢷⣕⠋⠀⠘⢿⡶⣤⣧⡉⠙⠓⣶⠿⣬⣀⣀⣐⡶⠋⣀⣀⣬⢾⢻⣿⠀⣼⠃
    ⠀⠀⠙⣦⠀⠀⠈⠳⣄⡟⠛⠿⣶⣯⣤⣀⣀⣏⣉⣙⣏⣉⣸⣧⣼⣾⣿⠀⡇⠀
    ⠀⠀⠀⠘⢧⡀⠀⠀⠈⠳⣄⡀⣸⠃⠉⠙⢻⠻⠿⢿⡿⢿⡿⢿⢿⣿⡟⠀⣧⠀
    ⠀⠀⠀⠀⠀⠙⢦⣐⠤⣒⠄⣉⠓⠶⠤⣤⣼⣀⣀⣼⣀⣼⣥⠿⠾⠛⠁⠀⢿⠀
    ⠀⠀⠀⠀⠀⠀⠀⠈⠙⠦⣭⣐⠉⠴⢂⡤⠀⠐⠀⠒⠒⢀⡀⠀⠄⠁⡠⠀⢸⠀
    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠙⠲⢤⣀⣀⠉⠁⠀⠀⠀⠒⠒⠒⠉⠀⢀⡾⠀
    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠉⠛⠲⠦⠤⠤⠤⠤⠴⠞⠋⠀⠀


";
//_______________________________________________________

// COMMANDS

//  The default configuration is set for awesome-WM and system d. this can be easily changed by replacing the commands here.
    if y == "face" {
        println!("{}", x)
    } else if y == "logout" {
        Command::new("awesome-client").arg("awesome.quit()").status().expect("failed to execute process");
    } else if y == "suspend" {
        Command::new("systemctl").arg("suspend").status().expect("failed to execute process");
    } else if y == "reboot" {
        Command::new("reboot").status().expect("failed to execute process");
    } else if y == "shutdown" {
        Command::new("shutdown").arg("-h").arg("now").status().expect("failed to execute process");
    } else {
            println!("exiting");

        let sleep_duration = Duration::from_millis(1500);
            thread::sleep(sleep_duration);
    }
}
// COMMAND LINE ARGUMENTS
// This allows you to add power menu buttons to an application menu by using the following arguments:
// shutdown, reboot, suspend, logout, or menu.
fn main() {
    let args: Vec<String> =env::args().collect();
        let mode =args[1].clone();

    match mode.trim() {
        "menu" => menu(),
        "shutdown" => shutdown(),
        "reboot" => reboot(),
        "suspend" => suspend(),
        "logout" => logout(),
        _ => menu(),
    }
}
// MENU
//  The default menu is set to 1-5 to access an action. this can easily be changed as needed.
fn menu() {
    commands("face");

println!("
                    (1) suspend
                    (2) logout
                    (3) reboot
                    (4) shutdown
                    (5) Cancel
                _________________
    ENTER ANY NUMBER 1 TO 4. CANCEL IS DEFAULT");

        let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line");

    match input.trim() {
        "1" => commands("suspend"),
        "2" => commands("logout"),
        "3" => commands("reboot"),
        "4" => commands("shutdown"),
        _ => commands("exit"),
    }
}
// LOGOUT
fn logout() {
    commands("face");
println!(" DO YOU WANT TO LOGOUT? PRESS ENTER TO CONTINUE. N + ENTER TO CANCEL");

    let mut input = String::new();
io::stdin().read_line(&mut input).expect("failed to read line");

    if input.trim() == "n" {
        commands("exit");
    } else if input.trim() == "N" {
        commands("exit");
    } else {
        commands("logout");
    }
}
// SUSPEND
fn suspend() {
    commands("face");
println!(" DO YOU WANT TO SUSPEND THE SYSTEM? PRESS ENTER TO CONTINUE. N + ENTER TO CANCEL");

    let mut input = String::new();
io::stdin().read_line(&mut input).expect("failed to read line");

    if input.trim() == "n" {
        commands("exit");
    } else if input.trim() == "N" {
        commands("exit");
    } else {
        commands("suspend");
    }
}
// REBOOT
fn reboot() {
    commands("face");
println!(" DO YOU WANT TO REBOOT? PRESS ENTER TO CONTINUE. N + ENTER TO CANCEL");

    let mut input = String::new();
io::stdin().read_line(&mut input).expect("failed to read line");

    if input.trim() == "n" {
        commands("exit");
    } else if input.trim() == "N" {
        commands("exit");
    } else {
        commands("reboot");
    }
}
// SHUTDOWN
fn shutdown() {
    commands("face");
println!(" DO YOU WANT TO SHUTDOWN? PRESS ENTER TO CONTINUE. N + ENTER TO CANCEL");

    let mut input = String::new();
io::stdin().read_line(&mut input).expect("failed to read line");

    if input.trim() == "n" {
        commands("exit");
    } else if input.trim() == "N" {
        commands("exit");
    } else {
        commands("shutdown");
    }
}
